<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>WEBSOCKET DEMO!</title>
</head>
<body>
<h2>WEBSOCKET DEMO!</h2><br/>

<script type="text/javascript" src="static/js/sockjs-1.1.1.js"></script>
<script type="text/javascript" src="static/js/jquery.min.js"></script>
<script>
    var websocket;
    if ('WebSocket' in window) {
        websocket = new WebSocket("ws://localhost:8080/ws");
    } else if ('MozWebSocket' in window) {
        websocket = new MozWebSocket("ws://localhost:8080/ws");
    } else {
        websocket = new SockJS("http://localhost:8080/sockjs/ws");//如果浏览器不支持websocket，再开启SockJS
    }
    websocket.onopen = function (evnt) {
        alert('ok, connection established!')
    };
    websocket.onmessage = function (evnt) {
        $("#msg_in").html($("#msg_in").html() + "<br/>" + "(<font color='red'>" + evnt.data + "</font>)")
    };
    websocket.onerror = function (evnt) {
        alert('naaah, connection error!')
    };
    websocket.onclose = function (evnt) {
        alert('haaaa, connection closed!')
    }

    $(function () {
        $("#btn").on('click', function () {
            websocket.send($("#msg_out").val());
        });
    });

</script>

<input type="text" id="msg_out"><br/>
<button id="btn">SEND</button>
<div id="msg_in"></div>


</body>
</html>
