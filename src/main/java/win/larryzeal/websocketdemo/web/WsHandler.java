package win.larryzeal.websocketdemo.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.ArrayList;
import java.util.List;

//TODO:创建一个WebSocket server很简单，只要实现WebSocketHandler接口，或者继承TextWebSocketHandler/BinaryWebSocketHandler即可。
public class WsHandler extends TextWebSocketHandler {
	Logger logger = LoggerFactory.getLogger(WsHandler.class);

	//TODO:注意，可能有失效的，也许应该使用弱引用。
	private static final List<WebSocketSession> sessionList = new ArrayList<>();//放置所有WebSocketSession

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception{
		logger.info(message.toString());
		session.sendMessage(new TextMessage("hello there~ are u " + message.getPayload() + "？"));
	}

	/**********************************************************/
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception{
		sessionList.add(session);//
		logger.info("connection established!");
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception{
		logger.info("connection closed!");
	}

	/*****************************以下是自定义方法*****************************/

	//由于拦截器将http session attr存入了websocket session，所以可以让http请求与websocket信息互动--就是http获取websocket的内容，或反之。
	public void sendMsgToAllClients(String msg){
		sessionList.forEach(session -> {
			try{
				if(session.isOpen()){
					session.sendMessage(new TextMessage(msg));
				}
			} catch(Exception ex){
				logger.info("发送至 " + session.getId() + " 的消息[" + msg + "]没有发送成功，原因是：" + ex.getMessage());
			}
		});
	}

	/**
	 * 这个需要与http session配合，否则无法知道是谁 -- 就是通过将client存入httpsession，然后拦截器再存入websocket session。
	 * TODO
	 *
	 * @param client
	 * @param msg
	 */
	public void sendMsgToClient(String client, String msg){

	}

	/**
	 * 返回当前websocket客户端的数量。
	 *
	 * @return
	 */
	public int getCount(){
		return sessionList.size();
	}

}
