package win.larryzeal.websocketdemo.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.WebSocketHandler;
import win.larryzeal.websocketdemo.web.WsHandler;

/**
 * 示例用。
 * Created by Larry on 2017/1/4.
 */
@RestController
@RequestMapping( "/sys" )
public class SystemController {
	@Autowired
	private WebSocketHandler wsHandler;

	/**
	 * 获取websocket客户端的数量。--不是严格准确的。
	 *
	 * @return
	 */
	@RequestMapping( "/count" )
	public int count(){
		return ((WsHandler) wsHandler).getCount();
	}

	/**
	 * TODO:仅作演示用，如果真需要的话，建议使用POST方式。
	 * 目的，给所有websocket客户端发送消息。
	 *
	 * @param msg
	 * @return
	 */
	@RequestMapping( "/all/{msg}" )
	public String send(@PathVariable String msg){
		((WsHandler) wsHandler).sendMsgToAllClients(msg);
		return "done!";
	}
}
