package win.larryzeal.websocketdemo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

/**
 * 负责注册除Controller等web层以外的所有bean，包括aop代理，service层，dao层，缓存，等等
 */
@Configuration

@ComponentScan(basePackages = "win.larryzeal", excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ANNOTATION, value = { Controller.class }) })

@EnableAspectJAutoProxy(proxyTargetClass = true)

public class AppConfig {


}
